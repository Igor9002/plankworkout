package com.development.phoenics.plank30days.data

import android.app.Application
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.development.phoenics.plank30days.data.database.TRAINING_DAYS
import com.development.phoenics.plank30days.data.database.TrainingDao
import com.development.phoenics.plank30days.data.database.TrainingRoomDatabase
import com.development.phoenics.plank30days.data.database.models.Training
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import java.util.*

class TrainingRepository(application: Application) {

    private var trainingDao: TrainingDao = TrainingRoomDatabase.getInstance(application)?.trainingDao()!!
    private val lastFinishedTrainingId = trainingDao.getFinishedTrainings()

    fun getRemainingTrainings(): LiveData<Int> {
        return trainingDao.getRemainingTrainingCount()
    }

    suspend fun getNextSession(): String {
        return coroutineScope {
            async {
                if (lastFinishedTrainingId == 0) {
                    return@async "Today"
                }
                val lastFinishedTraining = trainingDao.getTraining(lastFinishedTrainingId)

                val currentDate = Date(System.currentTimeMillis())
                val lastFinishedTrainingDate = lastFinishedTraining.currentDay?.let { Date(it) }

                if (currentDate.after(lastFinishedTrainingDate)) {
                    return@async "Today"
                }

                return@async "Tomorrow"
            }.await()
        }

    }

    fun accomplishSession(executionTime: Int) {
        trainingDao.accomplishSession(
            getNextSessionToAccomplish().toLong(),
            true,
            executionTime
        )
    }

    private fun getNextSessionToAccomplish(): Int {
        var nextTrainingId = 0
        if (lastFinishedTrainingId < TRAINING_DAYS) {
            nextTrainingId = lastFinishedTrainingId + 1
        }
        return nextTrainingId
    }

    fun resetTraining() {}
}