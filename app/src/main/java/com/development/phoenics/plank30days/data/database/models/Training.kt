package com.development.phoenics.plank30days.data.database.models

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "training")
data class Training(
    @PrimaryKey(autoGenerate = true)
    var id: Long?,
     var currentDay: Long?,
     var finished: Boolean,
     var executionTime: Int
) {
    constructor() : this(null, null, false, 0)
}