package com.development.phoenics.plank30days.data.database

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import com.development.phoenics.plank30days.data.database.models.Training

@Dao
interface TrainingDao {

    @Insert
    fun insert(training: Training)

    @Insert
    fun insertAll(training: MutableList<Training>)

    @Query("SELECT * from training")
    fun getAllTrainings(): List<Training>

    @Query("SELECT COUNT(finished) FROM training WHERE finished = 0")
    fun getRemainingTrainingCount(): LiveData<Int>

    @Query("SELECT COUNT(finished) FROM training WHERE finished = 1")
    fun getFinishedTrainings(): Int

    @Query("SELECT id, currentDay, finished, executionTime from training WHERE id = :id")
    fun getTraining(id: Int): Training

    @Query("UPDATE training SET finished = :finished, executionTime = :executionTime WHERE id = :id")
    fun accomplishSession(id: Long?, finished: Boolean, executionTime: Int)

}