package com.development.phoenics.plank30days.main

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.development.phoenics.plank30days.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val fragmentManager = supportFragmentManager
        val mainFragment = MainFragment.newInstance()
        fragmentManager.beginTransaction().add(R.id.container, mainFragment).commit()
    }
}
