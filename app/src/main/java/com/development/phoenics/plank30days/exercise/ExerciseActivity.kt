package com.development.phoenics.plank30days.exercise

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.development.phoenics.plank30days.R

class ExerciseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_exercise)
    }

    private fun setActivityResult() {
        val returnIntent = Intent()
        returnIntent.putExtra("exercise", 10)
        setResult(Activity.RESULT_OK, returnIntent)
        finish()
    }
}
