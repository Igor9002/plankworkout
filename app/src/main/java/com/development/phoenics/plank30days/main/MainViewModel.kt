package com.development.phoenics.plank30days.main

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import com.development.phoenics.plank30days.data.TrainingRepository

class MainViewModel(application: Application) : AndroidViewModel(application) {

    private val trainingRepository: TrainingRepository = TrainingRepository(application)

    fun getRemainingTrainingDays(): LiveData<Int> {
        return trainingRepository.getRemainingTrainings()
    }

    suspend fun getNextSession(): String {
        return trainingRepository.getNextSession()
    }
}
