package com.development.phoenics.plank30days.data.database

import android.arch.persistence.db.SupportSQLiteDatabase
import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import android.os.AsyncTask
import com.development.phoenics.plank30days.data.database.models.Training

const val TRAINING_DAYS = 30

@Database(entities = [Training::class], version = 2)
abstract class TrainingRoomDatabase : RoomDatabase() {

    abstract fun trainingDao(): TrainingDao

    companion object {
        private const val DATABASE_NAME = "training.db"

        // For Singleton instantiation
        private val LOCK = Any()
        @Volatile
        private var sInstance: TrainingRoomDatabase? = null

        fun getInstance(context: Context): TrainingRoomDatabase? {
            if (sInstance == null) {
                synchronized(LOCK) {
                    if (sInstance == null) {
                        sInstance = Room.databaseBuilder<TrainingRoomDatabase>(
                            context.applicationContext,
                            TrainingRoomDatabase::class.java, TrainingRoomDatabase.DATABASE_NAME
                        ).addCallback(object :Callback(){
                            override fun onCreate(db: SupportSQLiteDatabase) {
                                super.onCreate(db)
                                createTrainings(context)
                            }
                        }).build()
                    }
                }
            }
            return sInstance
        }

        private  fun createTrainings(context: Context) {
            TrainingsCreator().execute(context)
        }

        class TrainingsCreator : AsyncTask<Context, Void, Void>() {

            override fun doInBackground(vararg params: Context): Void? {
                val trainingsList = mutableListOf<Training>()
                val currentDateInMillis = System.currentTimeMillis()
                val dayInMillis = 24 * 60 * 60 * 100


                for (i in 0 until TRAINING_DAYS) {
                    val training = Training()
                    training.currentDay = currentDateInMillis + (i * dayInMillis)
                    training.executionTime = 0
                    training.finished = false
                    trainingsList.add(training)
                }

                val context = params[0]
                    getInstance(context)?.trainingDao()?.insertAll(trainingsList)
                return null
            }
        }
    }


}