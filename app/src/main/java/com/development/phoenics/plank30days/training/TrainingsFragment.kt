package com.development.phoenics.plank30days.training

import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.development.phoenics.plank30days.R
import com.development.phoenics.plank30days.exercise.ExerciseActivity

class TrainingsFragment : Fragment() {

    companion object {
        fun newInstance() = TrainingsFragment()
    }

    private lateinit var viewModel: TrainingsViewModel
    private val training = "training"

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.training_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(TrainingsViewModel::class.java)

        val intent = Intent(activity, ExerciseActivity::class.java)
        intent.putExtra(training, 1)
        startActivityForResult(intent, 1)
    }

}
